#v2 
#multiple devices can be used
#params------
$SD_ticket_number="SD-123"
$user_device_names = @("Hostname")
$global_policy_name = "Win-Defender-Policy"
$global_policy_Id = "d4ca75f6-adc3-4f0b-9aed-xxxxxxxxxxx"

#Connect------

Install-Module Microsoft.Graph.Beta -AllowClobber

Import-Module Microsoft.Graph.DeviceManagement
Import-Module Microsoft.Graph.DeviceManagement.Actions
Import-Module Microsoft.Graph.Identity.DirectoryManagement
Import-Module Microsoft.Graph.Groups
Connect-MgGraph -Scopes "DeviceManagementConfiguration.ReadWrite.All","Device.Read.All","Group.ReadWrite.All"
Select-MgProfile -Name "beta"
#-------------
$new_policy = New-MgDeviceManagementIntentCopy -DeviceManagementIntentId $global_policy_Id -Description "$SD_ticket_number $user_device_names" -DisplayName "Exclusion Settings $SD_ticket_number"
$user_devices=@()
foreach ($user_device_name in $user_device_names) {
    $user_devices += Get-MgDevice -Filter "DisplayName eq '$user_device_name'" -All
}
$new_group = New-MgGroup -Description "$SD_ticket_number $user_device_name" -DisplayName "Intune-Defender-Exclusion-$SD_ticket_number" -MailEnabled:$False -MailNickname "Intune-Defender-Exclusion-$SD_ticket_number" -SecurityEnabled
foreach ($user_device in $user_devices) {
    New-MgGroupMember -GroupId $new_group.Id -DirectoryObjectId $user_device.Id
}
$Assignment=@(
    @{
        Target = @{
            "@odata.type" = "#microsoft.graph.groupAssignmentTarget"
            groupId = $new_group.Id
        }
    }
)
Set-MgDeviceManagementIntent -DeviceManagementIntentId $new_policy.Id -Assignments $Assignment

#Check result-------
$policy= Get-MgDeviceManagementIntent -All -Filter "contains(DisplayName, '$SD_ticket_number')"
$policy_id = $policy.Id
$assigned_group_id = (Get-MgDeviceManagementIntentAssignment -DeviceManagementIntentId $policy_id).Target.AdditionalProperties.groupId
$assigned_group_name = (Get-MgGroupById -Ids $assigned_group_id).AdditionalProperties.displayName
$assigned_group_membs = (Get-MgGroupMember -GroupId $assigned_group_id).AdditionalProperties.ForEach({ $_.displayName})
$Res=@{
    "Policy"= $($policy.DisplayName)
    "Assigned group"= $($assigned_group_name)
    "Group members"= $($assigned_group_membs)
}
$Res | select *
Write-Host "TODO:" -ForegroundColor Yellow
Write-Host "-Modify the policy settings: $($policy.DisplayName)" -ForegroundColor Yellow
Write-Host "-Add to the exclusion groups in: $($global_policy_name)" -ForegroundColor Yellow
