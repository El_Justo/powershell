#Store and read secure pass
# Read-Host "Enter Password" -AsSecureString |  ConvertFrom-SecureString | Out-File ".\secure_pass.txt"
# $pass = Get-Content ".\secure_pass.txt" | ConvertTo-SecureString

$exclusionfile = ".\exclusions.txt"
$targetdomainfile = ".\domains.txt"
$logfile = ".\templog_.txt"
$exclusions= ""

"$(Get-Date -Format "dd/MM/yyyy HH:mm:ss") Script start" | Out-File -Append -FilePath $logfile
try {
    $exclusions = Get-Content $exclusionfile -ErrorAction Stop
    $domains = Get-Content $targetdomainfile -ErrorAction Stop
}
catch {
    write-host "Error reading files:"
    $Error[0].Exception.Message
    "$(Get-Date -Format "dd/MM/yyyy HH:mm:ss") $($Error[0].Exception.Message)" | Out-File -Append -FilePath $logfile
    Exit
}
$cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $cred_user,$cred_pass 

    "$(Get-Date -Format "dd/MM/yyyy HH:mm:ss") Getting grey agents" | Out-File -Append -FilePath $logfile
    $Agent = Get-SCOMClass -Name Microsoft.Windows.Computer
    $Objects = Get-SCOMMonitoringObject -class:$Agent | Where-Object { ($_.IsAvailable –eq $false) -and ($_.InMaintenanceMode -eq $false) -and ($domains -contains $_."[Microsoft.Windows.Computer].DomainDnsName".value)}
    $servers = $objects.displayname
    "$(Get-Date -Format "dd/MM/yyyy HH:mm:ss") Grey agents found: $($servers.count)" | Out-File -Append -FilePath $logfile
    $filtered_count = 0
    foreach ($server in $servers) {
        $filtered = $false
        foreach ($exclusion in $exclusions) {
            if ($server -like "$exclusion" -or $server -eq "") {
                $filtered = $true
                $filtered_count += 1
                Break
            }
        }
        if ($filtered) {Continue}

            write-host $server
            "$(Get-Date -Format "dd/MM/yyyy HH:mm:ss") Invoke-Command on $server" | Out-File -Append -FilePath $logfile
            try {
                $response = Invoke-Command -ComputerName $server -ScriptBlock {$(Get-Service -Name "HealthService" | Restart-Service -Force -Verbose) *>&1} -Credential $cred -ErrorAction Stop
                $response
                "$(Get-Date -Format "dd/MM/yyyy HH:mm:ss") result: $response" | Out-File -Append -FilePath $logfile
            }
            catch {
                $Error[0].Exception.Message
                "$(Get-Date -Format "dd/MM/yyyy HH:mm:ss") $($Error[0].Exception.Message)" | Out-File -Append -FilePath $logfile
            }
    }
    "$(Get-Date -Format "dd/MM/yyyy HH:mm:ss") Filtered servers: $filtered_count" | Out-File -Append -FilePath $logfile