$Server = "RDS-Host"
$userpath = "\\$Server\c$\users\"

Function Get-RDPUserSessions {
    $sessions = qwinsta /server $server| ?{ $_ -notmatch '^ SESSIONNAME' } | %{
      $item = "" | Select "Active", "SessionName", "Username", "Id", "State", "Type", "Device"
      $item.Active = $_.Substring(0,1) -match '>'
      $item.SessionName = $_.Substring(1,18).Trim()
      $item.Username = $_.Substring(19,20).Trim()
      $item.Id = $_.Substring(39,9).Trim()
      $item.State = $_.Substring(48,8).Trim()
      $item.Type = $_.Substring(56,12).Trim()
      $item.Device = $_.Substring(68).Trim()
      return $item
    }
    return $sessions
}

Write-Progress -Activity "Searching disabled user profiles" -Id 2 -Status "Search path: $($userpath)"

$usrs = Get-ChildItem -Path $userpath -Recurse -Directory -Depth 0 | Select -ExpandProperty name |
foreach {Get-ADUser -Filter "((SamAccountName -eq '$($_)') -or (DisplayName -like '$($_) *'))"} | 
where {$_.Enabled -eq $false }

$wmiprofiles = $usrs | 
select -ExpandProperty SID |
foreach {Get-WmiObject -Filter "Sid = '$($_.Value)'" -ComputerName $Server -Class Win32_UserProfile}

if ($wmiprofiles.Count -le 0) {  
  Write-Host "No disabled user profiles were found! Exiting..." -BackgroundColor Black -ForegroundColor Yellow
  Exit
}

Write-Host "Warning!" -BackgroundColor Black -ForegroundColor Yellow
$wmiprofiles.Localpath | Write-Host 
$message  = 'These profiles will be deleted'
$question = 'Are you sure you want to proceed?'

$choices = New-Object Collections.ObjectModel.Collection[Management.Automation.Host.ChoiceDescription]
$choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&Yes'))
$choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&No'))

if ($Host.UI.PromptForChoice($message, $question, $choices, 1) -eq 0) {
  Write-Host 'Confirmed' -BackgroundColor Black -ForegroundColor Yellow
  Write-Host 'Logging off sessions:' -BackgroundColor Black -ForegroundColor Yellow
  
  $sessions = Get-RDPUserSessions #| select -ExpandProperty Username
  foreach ($session in $sessions){
    if ($usrs.SamAccountName.Contains("$($session.Username)")) {
      Write-Host "Logging off $($session.Username)"
      logoff /server $Server $session.Id
    }
  }

  foreach ($wmiprofile in $wmiprofiles) {
    Write-Progress -Activity "Deleting" -Id 2 -Status "Current user: $($wmiprofile.LocalPath)" -PercentComplete ($wmiprofiles.IndexOf($wmiprofile)/$wmiprofiles.Count*100)
    $wmiprofile | Remove-WmiObject 
  }

} else {
  Write-Host 'Cancelled'
}