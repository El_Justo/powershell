#USAGE
# $g = Get-SCOMGroup -DisplayName "All Windows Computers"
# Get-SCOMGroupMembers -Group $g -Recursive | where {$_.FullName -like "*computer*"}
function Get-SCOMGroupMembers {
    param (
        $Group,
        [switch]$Recursive
        )

    $instances = $Group | Get-SCOMClassInstance #getting 1st level group members
    $c = $instances | where {$_.GetType().Name -ne "MonitoringObjectGroup"}

    if ($Recursive) {
        foreach ($member in $instances) { #check if member is a sub-group which should be recursively counted
            #if ($member.GetClasses().Base.Identifier -like "*Group||") {
            if ($member.GetType().Name -eq "MonitoringObjectGroup") {
                $c +=  (Get-SCOMGroupMembers -Group $member -Recursive) # in case the object in group is also a group, then don't count it itself (-1) and count the members of this subgroup instead
            }
        }
    }

return $c
} #end function Count-SCOMGroupMembers
