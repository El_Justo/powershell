param(
    [Parameter(Mandatory=$True, Position=0, ValueFromPipeline=$true)]
    [System.String]
    $User,

    [Parameter(Mandatory=$True, Position=1, ValueFromPipeline=$false)]
    [System.Boolean]
    $DisableMail
)

Import-Module ActiveDirectory

$usrs = Get-ADUser -Filter "DisplayName -like '*$User*'" -Properties mail
if ($usrs.Count -eq 0) {
    Write-Host "Nothing found!" -BackgroundColor Black -ForegroundColor Yellow
    Exit
}
#$usrs | ft -AutoSize
Write-Host "Warning!" -BackgroundColor Black -ForegroundColor Yellow
Write-Host "Target users:" 
$usrs | select Name,SAMaccountname,Mail | ft -AutoSize

$message  = 'Disabling users.'
$question = 'Confirm?'
$choices = New-Object Collections.ObjectModel.Collection[Management.Automation.Host.ChoiceDescription]
$choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&Yes'))
$choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&No'))
if ($Host.UI.PromptForChoice($message, $question, $choices, 1) -eq 0) {
    $usrs | foreach {Disable-ADAccount -Identity $_.samaccountname -WhatIf}
}

if ($DisableMail -eq $true) {
$Exchname = 'Exchange'
$session = Enter-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://$Exchname/PowerShell/ -Authentication Kerberos

$usrs | foreach {Set-CASMailbox -Identity $_.samaccountname -OWAEnabled $false -ActiveSyncEnabled $false -Whatif;
                Get-MobileDevice -Mailbox $_.samaccountname | foreach {Remove-MobileDevice -Identity $_.identity -Confirm:$false -Whatif}
                }
Exit
}