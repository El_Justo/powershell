$passLenght	= 8
function Get-RandomCharacters($length, $characters) { 
    $random = 1..$length | ForEach-Object { Get-Random -Maximum $characters.length } 
    $private:ofs="" 
    return [String]$characters[$random]
}
function Scramble-String([string]$inputString){     
    $characterArray = $inputString.ToCharArray()   
    $scrambledStringArray = $characterArray | Get-Random -Count $characterArray.Length     
    $outputString = -join $scrambledStringArray
    return $outputString 
}

$lowers = Get-Random -Minimum 1 -Maximum ($passLenght-2)
$uppers = Get-Random -Minimum 1 -Maximum ($passLenght-$lowers-1)
$numbers= ($passLenght-$lowers-$uppers)

$password = Get-RandomCharacters -length $lowers -characters 'abcdefghkmnprstuvwxyz'
$password += Get-RandomCharacters -length $uppers -characters 'ABCDEFGHKLMNPRSTUVWXYZ'
$password += Get-RandomCharacters -length $numbers -characters '123456789'
#$password += Get-RandomCharacters -length 1 -characters '!"§$%&/()=?}][{@#*+'
$password = Scramble-String $password

Write-Host $password