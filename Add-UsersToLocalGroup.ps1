param(
    [Parameter(Mandatory=$True, Position=0, ValueFromPipeline=$false)]
    [System.String]
    $CSV,

    [Parameter(Mandatory=$True, Position=1, ValueFromPipeline=$false)]
    [System.String]
    $LocalGroup
)

Import-Module ActiveDirectory

Import-Csv $CSV -Delimiter ";" |
 foreach {Invoke-Command -ComputerName $_.PC -ScriptBlock{Add-LocalGroupMember -Group $LocalGroup -Member "domain\$($_.SamAccount)" -WhatIf} }