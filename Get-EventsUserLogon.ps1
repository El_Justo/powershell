. .\Get-WinEventData.ps1

$startDate = Get-Date 20.03.20
$endTime= Get-Date 24.03.20

Get-ADDomainController -Filter * | foreach {
    Get-WinEvent -FilterHashtable @{LogName='Security';id=4624;StartTime=$startDate;EndTime=$endTime;Data='username'} -ComputerName $_.Hostname |
    Get-WinEventData | select TimeCreated,MachineName,EventDataTargetUserName,EventDataWorkstationName,EventDataIpAddress
} | select * | ft -AutoSize
