Import-Module Microsoft.Graph.DeviceManagement
Import-Module Microsoft.Graph.DeviceManagement.Actions
Import-Module Microsoft.Graph.Identity.DirectoryManagement
Import-Module Microsoft.Graph.Groups
Connect-MgGraph -Scopes "DeviceManagementConfiguration.ReadWrite.All","Device.Read.All","Group.ReadWrite.All"
Select-MgProfile -Name "beta"

$new_group_id = "1e9f5af5-9abb-49c3-9bf5-53ad0374da66"
$user_device_names = @(
    "DevName"
)
$new_group = Get-MgGroup -GroupId $new_group_id
foreach ($user_device_name in $user_device_names) {
    $user_device = Get-MgDevice -Filter "DisplayName eq '$user_device_name'" -All 
    Write-Host "$($user_device.DisplayName)"
    if ($null -eq $user_device) {
        Write-Host "---$user_device_name"
    } 
    foreach ($device in $user_device) {
        New-MgGroupMember -GroupId $new_group.Id -DirectoryObjectId $device.Id}
}