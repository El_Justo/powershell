param(
    [Parameter(Mandatory=$True, Position=0, ValueFromPipeline=$true)]
    [System.String]
    $Server
)

$Sessions = Get-SmbOpenFile -CimSession $Server | Out-GridView -OutputMode Multiple

if ($Sessions.Count -eq 0) {
    Write-Host "Nothing selected." -Backg;roundColor Black -ForegroundColor Yellow
    Exit
}
Write-Host "Warning!" -BackgroundColor Black -ForegroundColor Yellow
Write-Host "Target files:" 
$Sessions | ft -AutoSize

$message  = 'Releasing files.'
$question = 'Confirm?'
$choices = New-Object Collections.ObjectModel.Collection[Management.Automation.Host.ChoiceDescription]
$choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&Yes'))
$choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&No'))
if ($Host.UI.PromptForChoice($message, $question, $choices, 1) -eq 0) {
#if ($result -eq 'Yes') {

$Sessions | foreach {
    Close-SmbOpenFile -SessionId $_.SessionId -CimSession $Server -Force -Confirm:$false
        }
}