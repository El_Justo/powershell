Import-Module ActiveDirectory

#Variable  
$SQLServer = "SQL\Instance"  
$SQLDBName = "DB"  
#$uid ="domain\userID"  
#$pwd = "password123"
$DaysToShow = 7

$ExchDBList = "DBMAIL01","DBMAIL02","DBMAIL03","DBMAIL04"
$ExchArchDBList = "ArchiveDB01"
$Exchname = 'Exchange'
$dcname = 'dc.domain.com'

$ADFilialPath = @{
    "Архангельск" = "OU=Аргхангельск,OU=Main,DC=local,DC=domain,DC=com"
    "Астрахань" = "OU=Астрахань,OU=Main,DC=local,DC=domain,DC=com"
    "Барнаул"               ="OU=Барнаул,OU=Main,DC=local,DC=domain,DC=com"
    "Белгород"              ="OU=Белгород,OU=Main,DC=local,DC=domain,DC=com"
    "Брянск"                ="OU=Брянск,OU=Main,DC=local,DC=domain,DC=com"
    "Великий Новгород"      ="OU=Великий Новгород,OU=Main,DC=local,DC=domain,DC=com"
    "Владимир"              ="OU=Владимир,OU=Main,DC=local,DC=domain,DC=com"
    "Волгоград"             ="OU=Волгоград,OU=Main,DC=local,DC=domain,DC=com"
    "Волжский"              ="OU=Волжский,OU=Main,DC=local,DC=domain,DC=com"
    "Вологда"               ="OU=Вологда,OU=Main,DC=local,DC=domain,DC=com"
    "Воронеж"               ="OU=Воронеж,OU=Main,DC=local,DC=domain,DC=com"
    "Домодедово"            ="OU=Домодедово,OU=Main,DC=local,DC=domain,DC=com"
    "Екатеринбург"          ="OU=Екатеринбург,OU=Main,DC=local,DC=domain,DC=com"
    "Ижевск"                ="OU=Ижевск,OU=Main,DC=local,DC=domain,DC=com"
    "Иркутск"               ="OU=Иркутск,OU=Main,DC=local,DC=domain,DC=com"
    "Казань"                ="OU=Казань,OU=Main,DC=local,DC=domain,DC=com"
    "Калининград"           ="OU=Калининград,OU=Main,DC=local,DC=domain,DC=com"
    "Калуга"                ="OU=Калуга,OU=Main,DC=local,DC=domain,DC=com"
    "Каменка"               ="OU=Каменка,OU=Main,DC=local,DC=domain,DC=com"
    "Кемерово"              ="OU=Кемерово,OU=Main,DC=local,DC=domain,DC=com"
    "Киров"                 ="OU=Киров,OU=Main,DC=local,DC=domain,DC=com"
    "Контакты"              ="OU=Контакты,OU=Main,DC=local,DC=domain,DC=com"
    "Красноярск"            ="OU=Красноярск,OU=Main,DC=local,DC=domain,DC=com"
    "Курск"                 ="OU=Курск,OU=Main,DC=local,DC=domain,DC=com"
    "Курьеры"               ="OU=Курьеры,OU=Main,DC=local,DC=domain,DC=com"
    "Липецк"                ="OU=Липецк,OU=Main,DC=local,DC=domain,DC=com"
    "Лосево"                ="OU=Лосево,OU=Main,DC=local,DC=domain,DC=com"
    "Люберцы"               ="OU=Люберцы,OU=Main,DC=local,DC=domain,DC=com"
    "Магнитогорск"          ="OU=Магнитогорск,OU=Main,DC=local,DC=domain,DC=com"
    'Минеральные воды'      ="OU=Минеральные воды,OU=Main,DC=local,DC=domain,DC=com"
    "Москва"                ="OU=Москва,OU=Main,DC=local,DC=domain,DC=com"
    'Набережные челны'      ="OU=Набережные челны,OU=Main,DC=local,DC=domain,DC=com"
    "Нижний Новгород"       ="OU=Нижний Новгород,OU=Main,DC=local,DC=domain,DC=com"
    "Новороссийск"          ="OU=Новороссийск,OU=Main,DC=local,DC=domain,DC=com"
    "Новосибирск"           ="OU=Новосибирск,OU=Main,DC=local,DC=domain,DC=com"
    "Омск"                  ="OU=Омск,OU=Main,DC=local,DC=domain,DC=com"
    "Орел"                  ="OU=Орел,OU=Main,DC=local,DC=domain,DC=com"
    "Пенза"                 ="OU=Пенза,OU=Main,DC=local,DC=domain,DC=com"
    "Пермь"                 ="OU=Пермь,OU=Main,DC=local,DC=domain,DC=com"
    "Радумля"               ="OU=Радумля,OU=Main,DC=local,DC=domain,DC=com"
    "Ростов-на-Дону"        ="OU=Ростов-на-Дону,OU=Main,DC=local,DC=domain,DC=com"
    "Рязань"                ="OU=Рязань,OU=Main,DC=local,DC=domain,DC=com"
    "Самара"                ="OU=Самара,OU=Main,DC=local,DC=domain,DC=com"
    "Санкт-Петербург"       ="OU=Санкт-Петербург,OU=Main,DC=local,DC=domain,DC=com"
    "Саранск"               ="OU=Саранск,OU=Main,DC=local,DC=domain,DC=com"
    "Саратов"               ="OU=Саратов,OU=Main,DC=local,DC=domain,DC=com"
    "Серпухов"              ="OU=Серпухов,OU=Main,DC=local,DC=domain,DC=com"
    "Симферополь"           ="OU=Симферополь,OU=Main,DC=local,DC=domain,DC=com"
    "Смоленск"              ="OU=Смоленск,OU=Main,DC=local,DC=domain,DC=com"
    "Ставрополь"            ="OU=Ставрополь,OU=Main,DC=local,DC=domain,DC=com"
    "Тамань"                ="OU=Тамань,OU=Main,DC=local,DC=domain,DC=com"
    "Тамбов"                ="OU=Тамбов,OU=Main,DC=local,DC=domain,DC=com"
    "Тверь"                 ="OU=Тверь,OU=Main,DC=local,DC=domain,DC=com"
    "Темрюк"                ="OU=Темрюк,OU=Main,DC=local,DC=domain,DC=com"
    "Тимашевск"             ="OU=Тимашевск,OU=Main,DC=local,DC=domain,DC=com"
    "Тольятти"              ="OU=Тольятти,OU=Main,DC=local,DC=domain,DC=com"
    "Тула"                  ="OU=Тула,OU=Main,DC=local,DC=domain,DC=com"
    "Тюмень"                ="OU=Тюмень,OU=Main,DC=local,DC=domain,DC=com"
    "Ульяновск"             ="OU=Ульяновск,OU=Main,DC=local,DC=domain,DC=com"
    "Усть-Лабинск"          ="OU=Усть-Лабинск,OU=Main,DC=local,DC=domain,DC=com"
    "Уфа"                   ="OU=Уфа,OU=Main,DC=local,DC=domain,DC=com"
    "Центральный офис"      ="OU=Центральный офис,OU=Main,DC=local,DC=domain,DC=com"
    "Краснодар"             ="OU=Центральный офис,OU=Main,DC=local,DC=domain,DC=com"
    "Чебоксары"             ="OU=Чебоксары,OU=Main,DC=local,DC=domain,DC=com"
    "Челябинск"             ="OU=Челябинск,OU=Main,DC=local,DC=domain,DC=com"
    "Электросталь"          ="OU=Электросталь,OU=Main,DC=local,DC=domain,DC=com"
    "Ярославль"             ="OU=Ярославль,OU=Main,DC=local,DC=domain,DC=com"
}

$ExchRetentionList = @{
    "Оператор по работе с клиентами" = "Commerce-Archivation"
    "Менеджер по сопровождению клиентов" = "Commerce-Archivation"
    "Консультант" = "IT-Archivation"
    "Менеджер ИТ проектов" = "IT-Archivation"
    "Юрист" = "Urist-Archivation"
    "Директор" = "Management-Archivation"
    "Экономист" = "Finance-Archivation"
    "Оператор в АТП" = "Logist-Archivation"
    "Оператор" = "Logist-Archivation"
    "Диспетчер" = "Logist-Archivation"
    "Менеджер по логистике" = "Logist-Archivation"
    "Координатор отдела логистики" = "Logist-Archivation"
    "Оператор по обработке документов" = "Logist-Archivation"
    "Оператор по сбору документов" = "Logist-Archivation"
    "Менеджер по персоналу" = "Default MRM Policy"
}

#____________________________________
#SQL Query  
$DateLimit = (Get-Date).AddDays(-$DaysToShow).ToShortDateString()
$SqlQuery = "select * from employees where d1 > Convert(datetime, '$DateLimit' )  order by rec_id desc"  
$SqlConnection = New-Object System.Data.SqlClient.SqlConnection  
$SqlConnection.ConnectionString = "Server = $SQLServer; Database = $SQLDBName; Integrated Security = True;"  
$SqlCmd = New-Object System.Data.SqlClient.SqlCommand  
$SqlCmd.CommandText = $SqlQuery  
$SqlCmd.Connection = $SqlConnection  
$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter  
$SqlAdapter.SelectCommand = $SqlCmd   
#Creating Dataset  
$DataSet = New-Object System.Data.DataSet  
$SqlAdapter.Fill($DataSet)  
$Users = $DataSet.Tables[0] | Out-GridView -OutputMode Multiple -Title "Select user"
if ($Users.Count -eq 0) {
    Exit
}
Write-Host "Selected users:"
$users | select fullname,windowslogin,email,name_job,filial | ft -AutoSize

#________________________________________
$CreateADUsers = $false
Write-Host "Warning!" -BackgroundColor Black -ForegroundColor Yellow
$message  = 'Configuring users'
$question = 'Create AD Users?'
    $choices = New-Object Collections.ObjectModel.Collection[Management.Automation.Host.ChoiceDescription]
    $choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&Yes'))
    $choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&No'))

    if ($Host.UI.PromptForChoice($message, $question, $choices, 1) -eq 0) {
        Write-Host 'AD Users creation selected.'
        $result = @()
        foreach ($User in $Users) {
            $Pathprop = $ADFilialPath[$User.filial]
            if ($null -eq $Pathprop) { #Default OU
                $Pathprop = "OU=Main,DC=local,DC=domain,DC=com" }
            $groups = @( #Default Groups
                "RDP Access"
            )
            $groups += "$($User.filial)"
            $prop = @{Name            = $User.surname +" "+ $User.firstname;
                    GivenName         = $User.firstname;
                    SurName           = $User.surname;
                    DisplayName       = $User.surname +" "+ $User.firstname;
                    Path              = $Pathprop
                    Title             = $User.name_job;
                    SamAccountName    = $User.windowslogin;
                    UserPrincipalName = $User.windowslogin + "@domain.com";
                    AccountPassword   = (ConvertTo-SecureString $User.windowspwd -AsPlainText -Force);
                    Enabled           = $true;
                    Server            = $dcname;
                }
        
                $result += New-ADUser @prop
                foreach ($group in $groups) {
                    Add-ADGroupMember -Identity $group.Trim(" ","`r","`n") -Members $User.windowslogin -Server $dcname
                } 

                #$prop
        }
        #$result | select DisplayName,DistinguishedName
    }
    else {
        Write-Host 'Skipping AD Users creation'
        $CreateADUsers = $false}
#________________________________________________

$Createmail = $false
Write-Host "Warning!" -BackgroundColor Black -ForegroundColor Yellow
$message  = 'Configuring users'
$question = 'Create exchange mailboxes?'

    $choices = New-Object Collections.ObjectModel.Collection[Management.Automation.Host.ChoiceDescription]
    $choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&Yes'))
    $choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&No'))

    if ($Host.UI.PromptForChoice($message, $question, $choices, 1) -eq 0) {
        Write-Host 'Mailbox creation selected.'
        $Createmail = $true
        Write-Progress -Activity "Connecting to Exchange" -Id 2  -Status "Connecting...."
        $Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://$Exchname/PowerShell/ -Authentication Kerberos
        Import-PSSession $Session -DisableNameChecking -WarningAction SilentlyContinue
        Write-Progress -Activity "Connecting to Exchange" -Id 2  -Status "Connected!"
         
        $MBs = Get-ADUser -Filter {samaccountname -notlike "HealthMailbox*"} -Properties mail,homemdb | where {$_.mail -match "@" -and $_.samaccountname -notlike "SM_*" -and $_.homemdb -match 'CN'} | Group-Object Homemdb
        $Mailscount = foreach ($MB in $MBs){
        [string]$DB = $MB.Name
        [pscustomobject]@{
        Database = $DB.Substring(3,$DB.IndexOf(',')-3)
        Count = $MB.count }
        }
        $MailboxDB = $Mailscount | where {$_.Database -in $ExchDBList} | sort Count | select -First 1 | select -ExpandProperty Database
        Write-Host "Target MailboxDB = $MailboxDB"
        $ArchiveDB =  Get-MailboxDatabase -Status | where {$_.Name -in $ExchArchDBList} | sort AvailableNewMailboxSpace -Descending | select -First 1 |select -ExpandProperty Name
        Write-Host "Target ArchiveDB =  $ArchiveDB"
        $result = @()
        foreach ($User in $Users) {
            #Write-Progress -Activity "Creating AD Users" -Id 2 -Status "Current user: $($User.windowslogin)" -PercentComplete ($Users.IndexOf($User) / $Users.Count*100) -CurrentOperation "Creating mailbox"
            $alias_= $User.email.Split("@")[0]
            $UserPN = $User.windowslogin + "@domain.com";
            $RetPolicy = $ExchRetentionList[$User.name_job]
            if ($null -eq $RetPolicy) {
                $RetPolicy = "Another-Archivation"
            }
            Enable-Mailbox -Identity $UserPN -Alias $alias_ -Database  $MailboxDB -DomainController $dcname | out-null
            Enable-Mailbox -Identity $UserPN -Archive -ArchiveDatabase $ArchiveDB -DomainController $dcname | out-null
            Set-Mailbox -Identity $UserPN -RetentionPolicy $RetPolicy -DomainController $dcname | out-null
            $result += Get-Mailbox -Identity $UserPN -DomainController $dcname
            }
        $result | select DisplayName,PrimarySmtpAddress,Database,ArchiveDatabase,RetentionPolicy | ft -AutoSize
        }
    else {
        Write-Host 'Skipping mailbox creation'
        $Createmail = $false}