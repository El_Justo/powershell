$tenant_name = "tenant.onmicrosoft.com"

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Set-PSRepository PSGallery -InstallationPolicy Trusted
Install-Module MSAL.PS ; Import-Module MSAL.PS

$ModulePath = "C:\source\Intune-PowerShell-Management\Scenario Modules\Apps" ; gci $ModulePath -Recurse | Unblock-File ; import-module "$ModulePath\Microsoft.Graph.Intune.Apps.psd1"
$ModulePath = "C:\source\IntuneWin32App-master" ;                              gci $ModulePath -Recurse | Unblock-File ; import-module "$ModulePath\IntuneWin32App.psd1"

$SourceFolder = "C:\source\ZoomInstallerFull"
$SetupFile = "ZoomInstallerFull.msi"
$OutputFolder = "C:\source\"

$Detect_path = "C:\Program Files\Zoom\bin"
$Detect_file = "Zoom.exe"
$Detect_Version = "5.16.2.22807"

try { $NewWin32AppPackage = New-IntuneWin32AppPackage -SourceFolder $SourceFolder -SetupFile $SetupFile -OutputFolder $OutputFolder -Verbose -Force }
catch { Write-Host "Error creating .intunewin, check your pathes, can't continue"; Exit }
$IntuneWinFile = $NewWin32AppPackage.Path
$IntuneWinMetaData = Get-IntuneWin32AppMetaData -FilePath $IntuneWinFile
$DetectionRule = New-IntuneWin32AppDetectionRuleFile -Path $Detect_path -FileOrFolder $Detect_file -Version -VersionValue $Detect_Version -Operator greaterThanOrEqual

$RequirementRules = New-IntuneWin32AppRequirementRule `
                    -Architecture "x64" `
                    -MinimumSupportedWindowsRelease "W10_1607"
$newprops = @{
    DisplayName = "Zoom Meetings"
    AppVersion = "5.16.2"
    InformationURL = "https://zoom.us/"
    PrivacyURL = "https://explore.zoom.us/en/privacy/"
    InstallCommandLine= 'msiexec /i "ZoomInstallerFull.msi" /quiet /qn /norestart /log C:\Windows\Temp\Zoom_install.log ZoomAutoUpdate="true" MSIRESTARTMANAGERCONTROL="Disable"'
    UninstallCommandLine = "msiexec /x $($IntuneWinMetaData.ApplicationInfo.MsiInfo.MsiProductCode) /qn"
    FilePath = $NewWin32AppPackage.Path
    Developer= "Eric Yuan"
    InstallExperience= "system"
    Description= "Zoom is a video conferencing platform that can be used through a computer desktop or mobile app, and allows users to connect online for video conference meetings, webinars and live chat"
    Owner= "Telecom"
    RestartBehavior= "suppress"
    #ReturnCode= $ReturnCodes
    Publisher= "Zoom"
    #Notes= ""
    DetectionRule= $DetectionRule
    RequirementRule = $RequirementRules
    CategoryName = "Collaboration & Social"
    Icon= "iVBORw0KGgoAAAANSUhEUgAAANwAAAAyCAMAAAAAykVBAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAjdQTFRFAAAALIv/LIv/LYz/LYv/LYz/J4v/J4n/K4z/LIz/LIv/LYv/LIz/LIz/K4r/LIv/LYv/LYv/LYv/LIv/LIz/KIf/AID/LIn/LIr/LYz/LIz/LYv/LIv/LYz/LIv/LIv/LYr/KYr/LIr/LYv/LYv/LIz/LYv/LIv/LIv/LIr/LYz/AAD/KYz/K4v/LYz/K4j/LIv/LIz/LIv/Kor/JoT/LYv/LIv/LIv/LIT/JIn/K4r/LYv/LIz/K4r/LIv/LIz/J4n/LIv/LYv/LIr/LIz/JG3/Ioj/LIr/K4v/HHH/IID/LIv/LIv/K4D/LIv/LYv/LIv/K4v/LYr/LIz/Koz/LYv/K4r/LYz/LIv/LYr/LIX/LYz/LIv/LIv/LYz/AFX/LYv/GoD/LIz/LYv/LIv/LYv/LIv/LIz/LIz/LIv/LIv/LIv/LIz/LIv/LIr/LIz/K4f/K4D/LYv/K4r/LYz/IID/KYr/K4v/LIz/LYv/JIb/LYf/KYX/KIb/LYz/LIv/LIv/LIv/J4n/K4r/K4z/LIz/LYv/KYv/LYv/K4r/LIn/K4j/K4r/LYv/LYv/Kof/LIz/K4v/LIz/LIv/LIr/Kov/LYv/LIz/I4v/K4j/LYn/LYr/AID/K4n/LYv/LIz/LYr/LYv/K4j/K4n/KYz/K4r/LIr/K4n/LYn/K4r/LIr/LIv/K4v/LIr/LIv/LIr/K4z/LIv/LYv/LYz/LYv/K4z/LYb/LYz/LYz/LIz/K4z/LYz/////Md5t1gAAALt0Uk5TAPns3bt+IQ1UkKnC2/NT2o/w2K5oIAI0b6rQ4/f61ax4JSN3q+v0wKd09gEfcLwe/f6yPRt94M8dHHbNhRjcsxqY+5ebBw+MggkItW4SiupRTVVzSe5I/IaDF+97dZ8Dmgpdt6OwodfFxGPy5lZ6uiQMtlkzEDJY4nIVIhkm5IuteSdrKuhnLOlkKS9gyMEx7VpSS0w3OfEWPFA/BEH1+EpER04+O1c2W15iLmVp521xuaXZYYcoZqa4fCnzcEUAAAABYktHRLxK0uLvAAAACXBIWXMAAABIAAAASABGyWs+AAAFpElEQVRo3tWa/V8URRzH9w4QuAesSAS7Q4Kg7njwTg6Sq3jKOMUArRCJ7EooMgiBDJTS0EjUSnuUngS15yLLnsz2n+v29nZnvrszszPr3r1ezk+338/3MzvvndndudmRXDJPcbnz8gs2SIRSWFTs8fr8fp/XU1xSKHEVzbPR67mL7Ln7ntJ7N2kHZZvLK7bcFwi6K7dW3W/Ora55oLLWVbfxwYdCRtXNBZcu4fJ6g7mhsSmIZwSbGrdZkRk9rvxysyeSEqLbm5WfsZZWPP3hqh14Ylv8EVytfPQxXG3nh5PlDkDX2dVtTunu6mShET3Bx42eiCrsfKInETRmB3bt1tJ6C8JGtW4XNhT2iMDJTyJjX3+YnBPu76OhcXsycPKWAVJ26141a18eSR14Sq/maSG4cIPme2aQnjW4n8zG7xmyaEdCGUIHaOqwNsCeHRGiey5jOxhhZUVCJDYBjxWc/Hyy7AW6Wvpipp5DQnCj6lgfs8ob6zWiCXks4eSXtrLUUq3vXhaBG0+38xXrxMMGOjFPxDqZXYa1ml6d4DdNKgbLPlDKGIQT81j3nFV5TauqbepInpvvbe5X7h2+6sF9J+i57Z6TBzgmFBtaoScuSfvhmYe88ebpmZnp5rgXXu8I9vwT9RjgorOvH32jZ26+gwidUufM6rAlW/UxWM/x1LtqBA8EEgsoeyERwLVB/d0l7IEQs9Na9pvbzWwUte4tC7YTJ2E95anY23hgMQYNsUVc7dfCwh4cLlKAzYvqTxk6D6pTmHqAzXZ6Enb/O6lYJzbHiLYYJ5tSfUsU6eFONSjuwQdrAcyugnB0dZzJtvQuZDujBLuwwDzJNY+1tEsNiXuwDhgtM1wKMDJNagfSTjDYls9CtnNKsAGb97aQfS0oo7vBpgeDO2/MrcZbtcRQ43S292rh2H4/HW1EkcV6srEeu4cabXrQsPzAnHxBZqleXfRQ2S5+CNkOqeEmPRKI0awx9PxrsulBcAfNueUyn+qlnWwznMAPfaSGC9G/qwS90xN6UrDQngfBfWxO/URmqZ/qYjvlVJ+NALZubTJThHAXJGpZQG0rsedB99ycOXVZ5lPD5DNdWgFswRJNKLbuc6WgcV9sz4NI28yZn8ucKvE8X0xAti91pUIPMh5FkhTX0yrseRBcjzlzt8ypkk7zVR1gc+0jXd5mVkOb8c6y40HD8mtCapRTJYiXVwHb2hVM8+nhaVZDp/U0nz0P6rkkIZVXNWtX4RpO4Bou+vX4DKuhM3qa354nS3DXwCRdDl8F6p0Nd2UNsK1ehvIdPSy/gf/H/d8adK8uZfeBkg24vXB5d+I7ozH3rwLH4IrgUvfKJZMx9y9xp+C+h2wjP5iNJbrKP5Wy43Ec7ke4VjN4lGDM/cTZGbif4OKE+yKxDYJ/X/JtehyG+xmy1f5CbkTO/6w6AbceBWxnlylXWHDJYJtNj6Nwv0K260sSreRmgchJuN8Amjx5msqW86W924b7HbKdJHxVR6UfT+VclBX2OAcXgmzHqllsUh9YXeFcThf1oKcbqfm8qmRawm0l7sjASi4+hDjWc/B/N6243O03ptRdEiEuA/yEJejRGzhEurzoFk4y1dTBdb7zKsW/nrbn8OPjCAkOLYUnmWrqYJwfTpb/UOy9h60TTZ+NhTw6XCsJ7k8mHFIl0Z0of6Vbmu0P/voteoEEN8uEQ2rqoEYIbkXdfxSysVVDwKP33N+kilCLk0w1dRDzyyIl04bsbrLRLoPrH1I9sQkWHOJRjm4Kwe3J1JHV7VFaz02R6znHgpPWcTjJIwKHPi84trHN7Mn03JEkpZp/WXA6j3p0U2BkurFanNiSSPSkey5wvIxaS2bvDAX+jB+Dk9pO3Wrn3InigvUUlhRXqJtJK/g3k1p6Vmt9t/7bxKojVjPqq12j9eyOqRsp9X9KsHFfQCGB8gAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNS0wOS0yNFQxNzo0NDo0MiswMDowMKUlwpcAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTUtMDktMjRUMTc6NDQ6NDIrMDA6MDDUeHorAAAAAElFTkSuQmCC"
    #$app.largeIcon.value
}

try {
    Connect-MSIntuneGraph -TenantID $tenant_name
}
catch {
    Write-Host "Error connecting to Intune, can't continue"
    Exit
}

$newprops

$message  = "Target tenant: {$tenant_name}"
$question = 'Confirm?'
    $choices = New-Object Collections.ObjectModel.Collection[Management.Automation.Host.ChoiceDescription]
    $choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&Yes'))
    $choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&No'))

    if ($Host.UI.PromptForChoice($message, $question, $choices, 1) -eq 0) {
        Write-Host 'Applying.'
        Add-IntuneWin32App @newprops -Verbose -UseAzCopy
     }
    else {
        Write-Host 'Cancel'
        Exit
    }
