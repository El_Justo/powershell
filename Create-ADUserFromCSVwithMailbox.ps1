param(
    [Parameter(Mandatory=$True, Position=0, ValueFromPipeline=$false)]
    [System.String]
    $CSV,

    [Parameter(Mandatory=$True, Position=1, ValueFromPipeline=$false)]
    [System.Boolean]
    $SendEmail
)

Import-Module ActiveDirectory

#---------Set variables-------------------
$DBList = "WorkDB1","WorkDB2","WorkDB3" #TopManDB
$dcname = 'dc1.domain.com'
$Exchname = 'exch.domain.com'
#$SendEmail = $true                    
$EmailTo   = "admin@domain.com","admin2@domain.com"
$EmailFrom = "script@domain.com"   #matthew@domain 
$EmailSMTP = "cas.domain.com" #smtp server adress, DNS hostname.
$EmailSubject = "New AD User(s)"
#----------------------------------------


$Users = @(Import-Csv $CSV -Delimiter ";" -Encoding 1251) #or '-Encoding Default'
Write-Host "Creating AD users:"

$Users | select Name,SamAccountName,@{n='ExpirationDate';e={(Get-Date $_.AccountExpirationDate.Trim(" ","`r","`n"))}},Path  | ft * -AutoSize

#----------Ask create mailboxes-----------------
$Createmail = $false
Write-Host "Warning!" -BackgroundColor Black -ForegroundColor Yellow
$message  = 'Creating AD Users'
$question = 'Create exchange mailboxes?'

    $choices = New-Object Collections.ObjectModel.Collection[Management.Automation.Host.ChoiceDescription]
    $choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&Yes'))
    $choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&No'))

    if ($Host.UI.PromptForChoice($message, $question, $choices, 1) -eq 0) {
        Write-Host 'Mailbox creation selected.'
        $Createmail = $true
        Write-Progress -Activity "Connecting to Exchange" -Id 2  -Status "Connecting...."
        $Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://$Exchname/PowerShell/ -Authentication Kerberos
        Import-PSSession $Session -DisableNameChecking
        Write-Progress -Activity "Connecting to Exchange" -Id 2  -Status "Connected!"
        $MailboxDB = $DBList |
        ForEach-Object {Get-Mailbox -Database $_ -ResultSize:Unlimited} | Group-Object -Property:Database | Sort-Object -Property:Count | Select-Object -ExpandProperty Name -First 1
     }
    else {
        Write-Host 'Skipping mailbox creation'
        $Createmail = $false}

#------------Import user info from CSV and Create AD User-----------------
foreach ($User in $Users) {
    $prop = @{name             = $User.name.Trim(" ","`r","`n")
            GivenName         = $User.GivenName.Trim(" ","`r","`n")
            SurName           = $User.SurName.Trim(" ","`r","`n")
            DisplayName       = $User.name.Trim(" ","`r","`n")
            path              = $User."Path".Trim(" ","`r","`n")
            Company             = $User.Company.Trim(" ","`r","`n")
            Title               = $User.Title.Trim(" ","`r","`n")
            Description         = $User.Description.Trim(" ","`r","`n")
            SamAccountName    = $User.samaccountname.Trim(" ","`r","`n")
            UserPrincipalName = $User."samAccountName".Trim(" ","`r","`n") + "@domain.com"
            ScriptPath = $User.ScriptPath.Trim(" ","`r","`n")
            HomeDirectory = $User.HomeDirectory.Trim(" ","`r","`n")
            HomeDrive = $User.HomeDrive.Trim(" ","`r","`n")
            AccountPassword   = (ConvertTo-SecureString $User.Password.Trim(" ","`r","`n") -AsPlainText -Force)
            AccountExpirationDate = $User.AccountExpirationDate -eq "" -or $User.AccountExpirationDate -eq $null ? $null : (Get-Date $User.AccountExpirationDate.Trim(" ","`r","`n"))
            Enabled           = $true
            Server            = $dcname
        }

        Write-Progress -Activity "Creating AD Users" -Id 2 -Status "Current user: $($User.samaccountname)" -PercentComplete ($Users.IndexOf($User)/$Users.Count*100) -CurrentOperation "Creating AD user"
        New-ADUser @prop
        foreach ($group in $User.Groups.Split(',')) {
            Add-ADGroupMember -Identity $group.Trim(" ","`r","`n") -Members $User.samaccountname -Server $dcname
        } 

    if ($Createmail -eq $true)
    {
        Write-Progress -Activity "Creating AD Users" -Id 2 -Status "Current user: $($User.samaccountname)" -PercentComplete ($Users.IndexOf($User) / $Users.Count*100) -CurrentOperation "Creating mailbox"
        Enable-Mailbox -Identity $prop.UserPrincipalName -Database  $MailboxDB -DomainController $dcname | out-null
    }
}
if ($Createmail -eq $true) {Remove-PSSession $Session}

Write-Host "Checking AD Users created:" -BackgroundColor Black -ForegroundColor Yellow
$ADResult =  $Users | select SamAccountName,Password | foreach {
    $pass=$_.Password.Trim(" ","`r","`n")
    $User = Get-ADUser $_.SamAccountName -Properties * -Server $dcname | select Name,SamAccountName,Enabled,AccountExpirationDate,Disting*;
    $user | select Name,SamAccountName,Enabled,@{n="Password";e={$pass}},AccountExpirationDate,Disting*
}
$ADResult | ft -AutoSize

if ($SendEmail -and $ADResult -ne $null){
    Write-Host "Sending Email." -BackgroundColor Black -ForegroundColor Yellow
    $EmailBody = $ADResult | ConvertTo-Html | Out-String
    Send-MailMessage -To $EmailTo -From $EmailFrom -Subject $EmailSubject -BodyAsHtml $EmailBody -SmtpServer $EmailSMTP -Encoding utf8 -WarningAction SilentlyContinue #-attachment $Log 
}