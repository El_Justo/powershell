$tenant_name = "tenant.onmicrosoft.com"

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Set-PSRepository PSGallery -InstallationPolicy Trusted
Install-Module MSAL.PS ; Import-Module MSAL.PS
# Install-Module -Name PoshFunctions
# Import-Module -Name PoshFunctions 
$ModulePath = "C:\source\Intune-PowerShell-Management\Scenario Modules\Apps" ; gci $ModulePath -Recurse | Unblock-File ; import-module "$ModulePath\Microsoft.Graph.Intune.Apps.psd1"
$ModulePath = "C:\source\IntuneWin32App-master" ;                              gci $ModulePath -Recurse | Unblock-File ; import-module "$ModulePath\IntuneWin32App.psd1"

$SourceFolder = "C:\source\webexvdi"
$SetupFile = "webexvdi.msi"
$OutputFolder = "C:\source\"

$RequirementRules = New-IntuneWin32AppRequirementRule `
                    -Architecture "x64" `
                    -MinimumSupportedWindowsRelease "1607"
$newprops = @{
    DisplayName = "Cisco Webex Meetings Virtual Desktop Plug-in"
    AppVersion = "43.2.1.18"
    InformationURL = "https://www.webex.com"
    #PrivacyURL = ""
    InstallCommandLine= 'msiexec /i "webexvdi.msi" /q'
    UninstallCommandLine = "msiexec /x {AFAC244D-8C3C-D9B1-F84A-85DD994848EA} /q"
    #Developer= ""
    InstallExperience= "system"
    Description= "Cisco Webex Meetings Virtual Desktop Plug-in"
    #Owner= ""
    RestartBehavior= "suppress"
    #ReturnCode= $ReturnCodes
    Publisher= "Cisco Systems, Inc."
    #Notes= ""
    #DetectionRule= $DetectionRule
    RequirementRule = $RequirementRules
    #Icon= $TargetApp.Icon
}

try {
    Connect-MSIntuneGraph -TenantID $tenant_name
}
catch {
    Write-Host "Error connecting to Intune, can't continue"
    Exit
}
try {
    $NewWin32AppPackage = New-IntuneWin32AppPackage -SourceFolder $SourceFolder -SetupFile $SetupFile -OutputFolder $OutputFolder -Verbose -Force
}
catch {
    Write-Host "Error creating .intunewin, check your pathes, can't continue"
    Exit
}

$IntuneWinFile = $NewWin32AppPackage.Path
$IntuneWinMetaData = Get-IntuneWin32AppMetaData -FilePath $IntuneWinFile
$DetectionRule = New-IntuneWin32AppDetectionRuleMSI -ProductCode $IntuneWinMetaData.ApplicationInfo.MsiInfo.MsiProductCode

$newprops.Add("FilePath",$NewWin32AppPackage.Path)
$newprops.Add("DetectionRule",$DetectionRule)

$newprops

$message  = "Target tenant: {$tenant_name}"
$question = 'Confirm?'
    $choices = New-Object Collections.ObjectModel.Collection[Management.Automation.Host.ChoiceDescription]
    $choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&Yes'))
    $choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&No'))

    if ($Host.UI.PromptForChoice($message, $question, $choices, 1) -eq 0) {
        Write-Host 'Applying.'
        Add-IntuneWin32App @newprops -Verbose -UseAzCopy
     }
    else {
        Write-Host 'Cancel'
        Exit
    }
