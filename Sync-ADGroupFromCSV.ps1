param(
    [Parameter(Mandatory=$True, Position=0, ValueFromPipeline=$false)]
    [System.String]
    $AddCSV,
    [Parameter(Mandatory=$True, Position=1, ValueFromPipeline=$false)]
    [System.String]
    $RemoveCSV,
    [Parameter(Mandatory=$True, Position=2, ValueFromPipeline=$false)]
    [System.String]
    $Group,
    [Parameter(Mandatory=$True, Position=2, ValueFromPipeline=$false)]
    [System.String]
    $LogFile,
    [Parameter(Mandatory=$False, Position=3, ValueFromPipeline=$false)]
    [System.Boolean]
    $DeleteFiles,
    [Parameter(Mandatory=$False, Position=3, ValueFromPipeline=$false)]
    [System.Boolean]
    $SendEmail
)
$SendEmail=$false

if ($PSVersionTable.PSVersion.Major -ne 5) {
    Write-Host "For v5 Powershell only, other version may work in unexpected way!" ;
    Exit
}

$EmailTo   = "alerts@domain.com"
$EmailFrom = "noreply@domain.com"  
$EmailSMTP = "10.10.0.1" #smtp server adress, DNS hostname.
$EmailSubject = "VPN Group changed"

if (!(Test-Path $AddCSV) -or !(Test-Path $RemoveCSV)) {
    Write-Host "Error checking CSVs existence."
    Exit
}
try {Get-ADGroup $Group -ErrorAction Stop | Out-Null
}
catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException]{
    Write-Host "Error checking group '$Group' existence."
    Exit
}

$err=@()
$AddSucc=@()
$DelSucc=@()
$RemoveSucc=@()
$NewUsers= Import-Csv $AddCSV -Encoding Default -Delimiter ";" |
            select -ExpandProperty Windowslogin |
            foreach {
                    try {Get-ADUser $_; }
                    catch {$err+=$_.Exception.Message.Trim(" ","`r","`n")+ "`r`n"  } 
                    }
$DelUsers= Import-Csv $RemoveCSV -Encoding Default -Delimiter ";" |
            select -ExpandProperty Windowslogin |
            foreach {
                    try {Get-ADUser $_; }
                    catch {$err+=$_.Exception.Message.Trim(" ","`r","`n")+ "`r`n"  } 
                    }                           

$NewUsers | select -ExpandProperty SamAccountName |
            foreach {
                try {Add-ADGroupMember -Identity $Group -Members $_ -ErrorAction Stop;
                    $AddSucc+="$_"
                    }
                catch {$err+=$_.Exception.Message.Trim(" ","`r","`n")+"`r`n"  } 
                }

$DelUsers | select -ExpandProperty SamAccountName |
            foreach {
                try {Remove-ADGroupMember -Identity $Group -Members -Confirm:$false $_ -ErrorAction Stop;
                    $RemoveSucc+="$_"
                    }
                catch {$err+=$_.Exception.Message.Trim(" ","`r","`n")+"`r`n"  } 
                }                
if ($DeleteFiles) {
    try {Remove-Item -Path $AddCSV -Force -ErrorAction Stop
        if (!(Test-Path $AddCSV)) {
            $DelSucc+=$AddCSV+ "`r`n"
        }}
    catch {$err+=$_.Exception.Message.Trim(" ","`r","`n")+"`r`n"}
    try {Remove-Item -Path $RemoveCSV -Force -ErrorAction Stop
        if (!(Test-Path $RemoveCSV)) {
            $DelSucc+=$RemoveCSV+ "`r`n"
        }}
    catch {$err+=$_.Exception.Message.Trim(" ","`r","`n")+"`r`n"}
}

$EmailBody = "$(Get-Date)"+ "`r`n"
$EmailBody += "Users has been added to '$Group' group: "+ "`r`n"
$EmailBody += $AddSucc + "`r`n"+ "`r`n"
$EmailBody += "Users has been removed from '$Group' group: "+ "`r`n"
$EmailBody += $RemoveSucc + "`r`n"+ "`r`n"
$EmailBody += "Files has been removed:"+ "`r`n"
$EmailBody +=  $DelSucc
if ($err -ne $null) {
$EmailBody += "`r`n" +"Errors: " + "`r`n"+$err
}

Write-Host $EmailBody
$EmailBody | Out-File -FilePath $LogFile -Append

if ($SendEmail){
    Write-Host "Sending Email." -BackgroundColor Black -ForegroundColor Yellow
    Send-MailMessage -To $EmailTo -From $EmailFrom -Subject $EmailSubject -BodyAsHtml $EmailBody -SmtpServer $EmailSMTP -Encoding utf8 -WarningAction SilentlyContinue #-attachment $Log 
}