if (([System.AppDomain]::CurrentDomain.GetAssemblies() | Where {$_ -match "System.Drawing"}) -eq $null) {
    [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") #для MessageBox
}
if (([System.AppDomain]::CurrentDomain.GetAssemblies() | Where {$_ -match "System.Windows.Forms"}) -eq $null) {
    [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") #для MessageBox
}

$source = "\\FS\certificate\"
$pass = "123"

#=============================================================
$form = New-Object System.Windows.Forms.Form
$form.Text = $source
$form.Size = New-Object System.Drawing.Size(350,250)
$form.StartPosition = 'CenterScreen'
$form.FormBorderStyle = 'FixedDialog'

$OKButton = New-Object System.Windows.Forms.Button
$OKButton.Location = New-Object System.Drawing.Point(75,180)
$OKButton.Size = New-Object System.Drawing.Size(75,23)
$OKButton.Text = 'OK'
$OKButton.DialogResult = [System.Windows.Forms.DialogResult]::OK
$form.AcceptButton = $OKButton
$form.Controls.Add($OKButton)

$CancelButton = New-Object System.Windows.Forms.Button
$CancelButton.Location = New-Object System.Drawing.Point(150,180)
$CancelButton.Size = New-Object System.Drawing.Size(75,23)
$CancelButton.Text = 'Cancel'
$CancelButton.DialogResult = [System.Windows.Forms.DialogResult]::Cancel
$form.CancelButton = $CancelButton
$form.Controls.Add($CancelButton)

$label = New-Object System.Windows.Forms.Label
$label.Location = New-Object System.Drawing.Point(10,20)
$label.Size = New-Object System.Drawing.Size(280,20)
$label.Text = 'Install cert:'
$form.Controls.Add($label)

$listBox = New-Object System.Windows.Forms.ListBox
$listBox.Location = New-Object System.Drawing.Point(10,40)
$listBox.Size = New-Object System.Drawing.Size(260,20)
$listBox.Height = 145

Get-ChildItem -Path $source -File -Filter *.pfx | foreach {
    [void] $listBox.Items.Add($_.Name)    
}

$form.Controls.Add($listBox)

$form.Topmost = $true

$result = $form.ShowDialog()

if ($result -eq [System.Windows.Forms.DialogResult]::OK)
{
    $cert = $listBox.SelectedItem
    $cert
}
else {
    Write-Host("$result pressed")
    Exit
}

# #========================================================================

$dest01 = Join-Path -Path "C:\Users\" -ChildPath $env:USERNAME
$dest = Join-Path -Path $dest01 -ChildPath "Appdata\local\certpfx"
New-Item $dest -ItemType Directory -ErrorAction SilentlyContinue

        $sourcefile = Join-Path -Path $source -ChildPath $cert
        $destfile = Join-Path -Path $dest -ChildPath $cert
 #       $sourcefile
 #       $destfile
            if (Test-Path -LiteralPath $destfile)
                {
                    Write-Host ("Cert file exists at $destfile")
                    $time01 = (Get-Item $sourcefile).CreationTime
                    $time02 = (Get-Item $destfile).CreationTime
                    If ($time02 -lt $time01) 
                        {
                        Write-Host ("$sourcefile is newer")
                        try {
                            Write-Host ("Replacing old cert")
                            cd "C:\Program Files\Crypto Pro\CSP\"
                            $res = .\certmgr.exe -install -store uMy -file $sourcefile -pfx -silent -keep_exportable -pin $pass
                            #Import-PfxCertificate -FilePath $sourcefile -Password (ConvertTo-SecureString -String $pass -Force -AsPlainText) -CertStoreLocation cert:\CurrentUser\My
                            $errcode = $($res | select -Last 1)
                            If ($errcode -eq "[ErrorCode: 0x00000000]") 
                            {
                            [System.Windows.Forms.MessageBox]::Show("Сертификат установлен успешно.`n$errcode" , "Сообщение" , 0, [System.Windows.Forms.MessageBoxIcon]::Information) | Out-Null
                            Write-Host ("$errcode")
                            Copy-Item $sourcefile $destfile -Force -Confirm:$false
                            }
                            else {
                            [System.Windows.Forms.MessageBox]::Show("Возникла ошибка.`n$res" , "Сообщение" , 0, [System.Windows.Forms.MessageBoxIcon]::Warning) | Out-Null
                            Exit
                            }   
                            }
                        catch{ Read-Host}
                        }
                    else {                      
                         Write-Host ("$sourcefile is not updated.")
                        $yesno = [System.Windows.Forms.MessageBox]::Show("Сертификат уже был установлен. Переустановить?" , "Сообщение" , [System.Windows.Forms.MessageBoxButtons]::YesNo, [System.Windows.Forms.MessageBoxIcon]::Question);
                        if ($yesno -eq "Yes") {
                            try {
                                Write-Host ("Replacing old cert")
                                cd "C:\Program Files\Crypto Pro\CSP\"
                                $res = .\certmgr.exe -install -store uMy -file $sourcefile -pfx -silent -keep_exportable -pin $pass
                                $errcode = $($res | select -Last 1)
                                #Import-PfxCertificate -FilePath $sourcefile -Password (ConvertTo-SecureString -String $pass -Force -AsPlainText) -CertStoreLocation cert:\CurrentUser\My
                                If ($errcode -eq "[ErrorCode: 0x00000000]") 
                                {
                                [System.Windows.Forms.MessageBox]::Show("Сертификат установлен успешно.`n$errcode" , "Сообщение" , 0, [System.Windows.Forms.MessageBoxIcon]::Information) | Out-Null
                                Write-Host ("$errcode")
                                Copy-Item $sourcefile $destfile -Force -Confirm:$false
                                }
                                else {
                                [System.Windows.Forms.MessageBox]::Show("Возникла ошибка.`n$res" , "Сообщение" , 0, [System.Windows.Forms.MessageBoxIcon]::Warning) | Out-Null
                                Exit
                                }   
                                }
                            catch{ Read-Host}
                        }
                        else {
                            Exit
                        }
                        
                    }
                }
                else {
                    Write-Host ("$destfile does not exist")
                    try {
                            Write-Host ("Installing new cert")
                            cd "C:\Program Files\Crypto Pro\CSP\"
                            $res = .\certmgr.exe -install -store uMy -file $sourcefile -pfx -silent -keep_exportable -pin $pass
                            #Import-PfxCertificate -FilePath $sourcefile -Password (ConvertTo-SecureString -String $pass -Force -AsPlainText) -CertStoreLocation cert:\CurrentUser\My
                            $errcode = $($res | select -Last 1)
                            If ($errcode -eq "[ErrorCode: 0x00000000]") 
                            {
                            [System.Windows.Forms.MessageBox]::Show("Сертификат установлен успешно.`n$errcode" , "Сообщение" , 0, [System.Windows.Forms.MessageBoxIcon]::Information) | Out-Null
                            Write-Host ("$errcode")
                            Copy-Item $sourcefile $destfile -Force -Confirm:$false
                            }
                            else {
                            [System.Windows.Forms.MessageBox]::Show("Возникла ошибка.`n$res" , "Сообщение" , 0, [System.Windows.Forms.MessageBoxIcon]::Warning) | Out-Null
                            Exit
                            }
                        }
                        catch{ Read-Host}
                }