$tenant_name = "tenant.onmicrosoft.com"

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Set-PSRepository PSGallery -InstallationPolicy Trusted
Install-Module MSAL.PS ; Import-Module MSAL.PS

$ModulePath = "C:\source\Intune-PowerShell-Management\Scenario Modules\Apps" ; gci $ModulePath -Recurse | Unblock-File ; import-module "$ModulePath\Microsoft.Graph.Intune.Apps.psd1"
$ModulePath = "C:\source\IntuneWin32App-master" ;                              gci $ModulePath -Recurse | Unblock-File ; import-module "$ModulePath\IntuneWin32App.psd1"


$SourceFolder = "C:\source\ODT"
$SetupFile = "setup.exe"
$OutputFolder = "C:\source\"

try { $NewWin32AppPackage = New-IntuneWin32AppPackage -SourceFolder $SourceFolder -SetupFile $SetupFile -OutputFolder $OutputFolder -Verbose -Force }
catch { Write-Host "Error creating .intunewin, check your pathes, can't continue"; Exit }

$DetectionRule = New-IntuneWin32AppDetectionRuleRegistry -KeyPath "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\O365ProPlusRetail - en-us" -VersionComparison -ValueName "DisplayVersion" -VersionComparisonOperator greaterThanOrEqual -VersionComparisonValue "16.0.16924.20106"

$RequirementRules = New-IntuneWin32AppRequirementRule `
                    -Architecture "x64" `
                    -MinimumSupportedWindowsRelease "W10_1607"
$newprops = @{
    DisplayName = "Microsoft 365 Apps"
    AppVersion = "16.0.16924.20106"
    InformationURL = "https://products.office.com/en-us/explore-office-for-home"
    PrivacyURL = "https://privacy.microsoft.com/en-US/privacystatement"
    InstallCommandLine= "setup.exe /configure install.xml"
    UninstallCommandLine = "setup.exe /configure uninstall.xml"
    FilePath = $NewWin32AppPackage.Path
    Developer= "Microsoft"
    InstallExperience= "system"
    Description= "Microsoft 365 Apps for Windows 10"
    Owner= "DXC SSC"
    RestartBehavior= "suppress"
    #ReturnCode= $ReturnCodes
    Publisher= "Microsoft Corporation"
    #Notes= ""
    DetectionRule= $DetectionRule
    RequirementRule = $RequirementRules
    CategoryName = "Productivity"
    #Icon= ""
    #$app.largeIcon.value
}

try {
    Connect-MSIntuneGraph -TenantID $tenant_name
}
catch {
    Write-Host "Error connecting to Intune, can't continue"
    Exit
}

$newprops

$message  = "Target tenant: {$tenant_name}"
$question = 'Confirm?'
    $choices = New-Object Collections.ObjectModel.Collection[Management.Automation.Host.ChoiceDescription]
    $choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&Yes'))
    $choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&No'))

    if ($Host.UI.PromptForChoice($message, $question, $choices, 1) -eq 0) {
        Write-Host 'Applying.'
        Add-IntuneWin32App @newprops -Verbose -UseAzCopy
     }
    else {
        Write-Host 'Cancel'
        Exit
    }
