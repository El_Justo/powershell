
#Variable to hold variable  
$SQLServer = "SQL\Instance"  
$SQLDBName = "DB"
#$uid ="domain\userID"  
#$pwd = "password123"   

#SQL Query  
$SqlQuery = "select a.windowslogin,a.fullname,isnull(a.phonecorp, case when d.id_manager is null then a.phonepersonal else '' end) as Phone
from employees a left join crf_noshowphone d on a.rec_id=d.id_manager
where a.d2 is null and a.windowslogin != ''
order by windowslogin"

$SqlConnection = New-Object System.Data.SqlClient.SqlConnection  
$SqlConnection.ConnectionString = "Server = $SQLServer; Database = $SQLDBName; Integrated Security = True;"  
$SqlCmd = New-Object System.Data.SqlClient.SqlCommand  
$SqlCmd.CommandText = $SqlQuery  
$SqlCmd.Connection = $SqlConnection  
$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter  
$SqlAdapter.SelectCommand = $SqlCmd   
$DataSet = New-Object System.Data.DataSet  
$SqlAdapter.Fill($DataSet)  
#$DataSet.Tables[0] | Out-GridView
foreach ($record in $DataSet.Tables[0] | select) {
    Set-ADUser $record.windowslogin -MobilePhone "+$($record.Phone)" -WhatIf
}