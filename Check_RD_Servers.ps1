﻿$servers = Get-WMIObject -ComputerName "RDS-Host" -Class Win32_SessionDirectoryServer -Filter "ClusterName='TERMINAL'"
$count = $servers.Count
$MessageBody = @"
Current collection members count: $count
"@
write-host $servers.count
if ($servers.Count -lt 30) {
 
$from = new-object System.Net.Mail.MailAddress("rds@domain.com", "Collection monitor")
Send-MailMessage -From $from -To "admin@domain.com" -Subject 'Collection problem' -Body $MessageBody -Priority High -SmtpServer 'exch.domain.com' -Encoding UTF8
 } 