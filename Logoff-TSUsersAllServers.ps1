
if (([System.AppDomain]::CurrentDomain.GetAssemblies() | Where {$_ -match "System.Drawing"}) -eq $null) {
    [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") #для MessageBox
}
if (([System.AppDomain]::CurrentDomain.GetAssemblies() | Where {$_ -match "System.Windows.Forms"}) -eq $null) {
    [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") #для MessageBox
}

#========================================================================
#$Sessions = Get-RDUserSession -ConnectionBroker $Server
$Servers = Get-ADComputer -Filter { OperatingSystem -Like '*Windows Server*'} | select -ExpandProperty Name | Out-GridView -OutputMode Multiple -Title "Выбор серверов для поиска сеансов. (Можно выбрать несколько)"
$Sessions = @()
foreach ($Server in $Servers) {
Write-Host -NoNewLine "Quering: $Server "
       
Get-TSSession -ComputerName $Server -ErrorAction SilentlyContinue -OutVariable TSSessions > temp #redirecting to a file, bc ipaddress disappears without it, $null doesnt help
$hive = [Microsoft.Win32.RegistryHive]::LocalMachine
$base = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey($hive, $Server)
$regpath = "SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList"
foreach ($TSSession in $TSSessions) {
    Write-Host -NoNewLine "."
    $login = $TSSession | select -ExpandProperty UserAccount
    $login = ($login -eq $null) ? "" : $login.ToString().Split("\")[1]
    $user = Get-ADUser $login -ErrorAction SilentlyContinue
    $Keypath = "SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\$($user.SID)"
    $key = $base.OpenSubKey($KeyPath)
    $profilePath = $key.GetValue("ProfileImagePath")

    $Sessions += [PSCustomObject]@{AccountName = $login
              Name = $user.Name
              Enabled = $user.Enabled
              Server = $Server
              Id = $TSSession.SessionId
              Status = $TSSession.ConnectionState
              IdleTime = $TSSession.IdleTime
              LoginTime = $TSSession.LoginTime
              ConnectTime = $TSSession.ConnectTime
              IPAddress = $($TSSession | select -ExpandProperty IpAddress | select -ExpandProperty IPAddressToString)
              ClientName = $TSSession.ClientName 
              ProfilePath = $profilePath
            }
    }
    Write-Host ""
}


$Sessions = $Sessions | Out-GridView -OutputMode Multiple -Title "Select session"

if ($Sessions.Count -eq 0) {
    [System.Windows.Forms.MessageBox]::Show("Nothing selected." , "Message" , 0, [System.Windows.Forms.MessageBoxIcon]::Warning) | Out-Null
    Exit
}
Write-Host "Warning!" -BackgroundColor Black -ForegroundColor Yellow
Write-Host "Target users:" 
$Sessions | ft -AutoSize

    $message  = 'Logging off users.'
    $question = 'Confirm?'
    $choices = New-Object Collections.ObjectModel.Collection[Management.Automation.Host.ChoiceDescription]
    $choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&Yes'))
    $choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&No'))
    if ($Host.UI.PromptForChoice($message, $question, $choices, 1) -eq 0) {
    #if ($result -eq 'Yes') {

    $Sessions | foreach -Parallel  {
        Stop-TSSession -ComputerName $_.Server -Id $_.Id -Force;
        Write-Host "$($_.Name) is logging off from $($_.Server)."
            }
    }